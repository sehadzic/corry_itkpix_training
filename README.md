The following steps describe the installation of the Corryvreckan software for the reconstruction and analysis of the ATLAS ITk Pixel test-beam data, assuming the installation on `lxplus`. More installation options are given in the Corryvreckan [user-manual](http://cern.ch/go/db9Z).

Download Corryvreckan and set the environment variables:
```
$ git clone -b master https://gitlab.cern.ch/sehadzic/corryvreckan.git
$ source corryvreckan/etc/setup_lxplus.sh
```
Download & install EUDAQ1 to be able to read the test-beam data:
```
$ git clone -b v1.x-dev https://github.com/sejlahadzic/eudaq.git
$ cd eudaq/build 
$ cmake ..
$ make install -j4
$ export EUDAQPATH=/path/to/your/eudaq
$ cd ../../
```
With EUDAQ1 installed the Corryvreckan installation can be completed with EventLodaerEUDAQ module:
```
$ cd corryvreckan
$ mkdir build && cd build
$ cmake .. -DBUILD_EventLoaderEUDAQ=ON
$ make install -j4
$ cd ../../
$ source corryvreckan/etc/setup_lxplus.sh
```
The script `corryvreckan/etc/setup_lxplus.sh` needs to be sourced every time after logging into `lxplus`.
