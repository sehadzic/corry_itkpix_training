[plane0]
mask_file = "../output/MaskCreator/plane0/mask_plane0.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 3.40354deg,-1.45056deg,0.366521deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -797.811um,-224.835um,0
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane1]
mask_file = "../output/MaskCreator/plane1/mask_plane1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 3.08217deg,-1.12512deg,-0.11293deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -691.533um,-194.566um,111mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane2]
mask_file = "../output/MaskCreator/plane2/mask_plane2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.51609deg,-0.818069deg,0.119003deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -454.158um,-282.454um,222mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane110]
mask_file = "../output/MaskCreator/plane110/mask_plane110.txt"
material_budget = 0.003
number_of_pixels = 200, 384
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 100um,25um
position = -1.21mm,-630um,355mm
role = "dut"
#spatial_resolution = 28.9um,7.2um
time_resolution = 200ns
type = "rd53a"

[plane112]
mask_file = "../output/MaskCreator/plane112/mask_plane112.txt"
material_budget = 0.003
number_of_pixels = 400, 192
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 50um,50um
position = -1.23mm,870um,425mm
role = "dut"
#spatial_resolution = 14.4um,14.4um
time_resolution = 200ns
type = "rd53a"

[plane3]
mask_file = "../output/MaskCreator/plane3/mask_plane3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,571mm
role = "reference"
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane4]
mask_file = "../output/MaskCreator/plane4/mask_plane4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.0876625deg,-0.680158deg,0.244309deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -265.695um,147.54um,681mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane5]
mask_file = "../output/MaskCreator/plane5/mask_plane5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0.566713deg,0.337415deg,-0.0403935deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -466.89um,240.273um,793mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

[plane21]
mask_file = "../output/MaskCreator/plane21/mask_plane21.txt"
material_budget = 0.003
number_of_pixels = 80, 336
orientation = 180deg,0,90deg
orientation_mode = "xyz"
pixel_pitch = 250um,50um
position = -1.39mm,530um,835mm
role = "dut"
#spatial_resolution = 72um,14um
time_resolution = 25ns
type = "fei4"
